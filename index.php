<?php
require("animal.php");

$sheep = new Animal("shaun");

echo "Nama Hewan: $sheep->name <br>"; // "shaun"
echo "Jumlah kaki hewan: $sheep->legs <br>"; // 2
echo "Berdarah dingin? $sheep->cold_blooded <br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

?>
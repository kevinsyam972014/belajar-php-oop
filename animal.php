<?php

class Animal
{
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";
    public function __construct($name){
        $this->name=$name;
    }
}

class Ape extends Animal
{
    public function yell(){
        echo "Auooo <br>";
    }
}

class Frog extends Animal
{
    public function jump(){
        echo "hop hop <br>";
    }
}

?>